<!DOCTYPE html>
<html class="no-js" lang="zxx">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Droitix | Web and Graphic Designs</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico">

    <!-- CSS
        ============================================ -->
    <!-- CSS
        ============================================ -->

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css"> -->

    <!-- Font Family CSS -->
    <!-- <link rel="stylesheet" href="assets/css/vendor/cerebrisans.css"> -->

    <!-- FontAwesome CSS -->
    <!-- <link rel="stylesheet" href="assets/css/vendor/fontawesome-all.min.css"> -->

    <!-- Swiper slider CSS -->
    <!-- <link rel="stylesheet" href="assets/css/plugins/swiper.min.css"> -->

    <!-- animate-text CSS -->
    <!-- <link rel="stylesheet" href="assets/css/plugins/animate-text.css"> -->

    <!-- Animate CSS -->
    <!-- <link rel="stylesheet" href="assets/css/plugins/animate.min.css"> -->

    <!-- Light gallery CSS -->
    <!-- <link rel="stylesheet" href="assets/css/plugins/lightgallery.min.css"> -->

    <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->

    <link rel="stylesheet" href="assets/css/vendor/vendor.min.css">
    <link rel="stylesheet" href="assets/css/plugins/plugins.min.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body>


    <div class="preloader-activate preloader-active open_tm_preloader">
        <div class="preloader-area-wrap">
            <div class="spinner d-flex justify-content-center align-items-center h-100">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>









    <!--====================  header area ====================-->
    <div class="header-area bg-white header-sticky only-mobile-sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header position-relative">
                        <!-- brand logo -->
                        <div class="header__logo">
                            <a href="index.html">
                                <img src="assets/images/logo/logo-dark.png" height="100" class="img-fluid" alt="">
                            </a>
                        </div>

                        <div class="header-right flexible-image-slider-wrap">

                            <div class="header-right-inner" id="hidden-icon-wrapper">
                                <!-- Header Search Form -->
                                <div class="header-search-form d-md-none d-block">
                                    <form action="#" class="search-form-top">
                                        <input class="search-field" type="text" placeholder="Search…">
                                        <button class="search-submit">
                                            <i class="search-btn-icon fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>

                                <!-- Header Top Info -->
                                <div class="swiper-container header-top-info-slider-werap top-info-slider__container">
                                    <div class="swiper-wrapper header-top-info-inner">
                                        <div class="swiper-slide">
                                            <div class="info-item">
                                                <div class="info-icon">
                                                    <span class="fa fa-phone"></span>
                                                </div>
                                                <div class="info-content">
                                                    <h6 class="info-title">0678579459</h6>
                                                    <div class="info-sub-title">design@droitix.co.za</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="info-item">
                                                <div class="info-icon">
                                                    <span class="fa fa-map-marker-alt"></span>
                                                </div>
                                                <div class="info-content">
                                                    <h6 class="info-title">4 Magnolia Rd, Attlasville</h6>
                                                    <div class="info-sub-title">Boksburg</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="info-item">
                                                <div class="info-icon">
                                                    <span class="fa fa-clock"></span>
                                                </div>
                                                <div class="info-content">
                                                    <h6 class="info-title">8:00AM - 5:00PM</h6>
                                                    <div class="info-sub-title">Monday to Saturday</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="info-item">
                                                <div class="info-icon">
                                                    <span class="fa fa-comment-alt-lines"></span>
                                                </div>
                                                <div class="info-content">
                                                    <h6 class="info-title">Online 24/7</h6>
                                                    <div class="info-sub-title">+27678579459</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Header Social Networks -->
                                <div class="header-social-networks style-icons">
                                    <div class="inner">
                                        <a class=" social-link hint--bounce hint--bottom-left" aria-label="Twitter" href="https://twitter.com/droitixweb" data-hover="Twitter" target="_blank">
                                            <i class="social-icon fab fa-twitter"></i>
                                        </a>
                                        <a class=" social-link hint--bounce hint--bottom-left" aria-label="Facebook" href="https://facebook.com/droitixweb" data-hover="Facebook" target="_blank">
                                            <i class="social-icon fab fa-facebook-f"></i>
                                        </a>
                                        <a class=" social-link hint--bounce hint--bottom-left" aria-label="Instagram" href="https://instagram.com/droitixweb" data-hover="Instagram" target="_blank">
                                            <i class="social-icon fab fa-instagram"></i>
                                        </a>
                                        <a class=" social-link hint--bounce hint--bottom-left" aria-label="Linkedin" href="https://linkedin.com/droitixweb" data-hover="Linkedin" target="_blank">
                                            <i class="social-icon fab fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <!-- mobile menu -->
                            <div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
                                <i></i>
                            </div>
                            <!-- hidden icons menu -->
                            <div class="hidden-icons-menu d-block d-md-none" id="hidden-icon-trigger">
                                <a href="javascript:void(0)">
                                    <i class="far fa-ellipsis-h-alt"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-bottom-wrap border-top d-md-block d-none header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header-bottom-inner">
                            <div class="header-bottom-left-wrap">
                                <!-- navigation menu -->
                                <div class="header__navigation d-none d-xl-block">
                                    <nav class="navigation-menu">
                                        <ul>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Home</span></a>

                                            </li>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Website Design</span></a>

                                            </li>

                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Graphic Designing</span></a>

                                            </li>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Web Systems Design</span></a>

                                            </li>
                                                  <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Online Stores</span></a>

                                            </li>

                                        </ul>
                                    </nav>
                                </div>
                            </div>

                            <div class="header-search-form">
                                <form action="#" class="search-form-top style-02">
                                    <input class="search-field" type="text" placeholder="Search…">
                                    <button class="search-submit">
                                        <i class="search-btn-icon fa fa-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--====================  End of header area  ====================-->















    <div id="main-wrapper">
        <div class="site-wrapper-reveal">
            <div class="bg-white">
                <!--============ Cybersecurity Hero Start ============-->
                <div class="cybersecurity-hero processing-hero-bg__color ">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!--baseline-->
                            <div class="col-lg-6 col-md-7">
                                <div class="cybersecurity-hero-text wow move-up">
                                    <h6>Say it, we make it! </h6>
                                    <h3 class="font-weight--reguler mb-30">We create websites <span class="text-color-primary">that</span> expose you <span class="text-color-primary">more</span> to your customers | From R950</h3>
                                    <div class="hero-button mt-30">
                                        <a href="#" class="ht-btn ht-btn-md">Find out More </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-5">
                                <div class="cybersecurity-hero-images-wrap wow move-up">
                                    <div class="cybersecurity-hero-images section-space--mt_80">
                                        <div class="inner-img-one">
                                            <img class="img-fluid worldRotate" src="assets/images/hero/mitech-slider-cybersecurity-global-image.png" alt="">
                                        </div>
                                        <div class="inner-img-two">
                                            <img class="img-fluid  ml-5 " src="assets/images/hero/slider-cybersecurity-slide-01-image-01.png" width="400" height="600" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--============ Cybersecurity Hero End ============-->

                <!--===========  feature-icon-wrapper  Start =============-->
                <div class="feature-icon-wrapper bg-gray section-space--ptb_100">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-title-wrap text-center section-space--mb_40">
                                    <h6 class="section-sub-title mb-20">What we do</h6>
                                    <h3 class="heading">Let us put you on the limelight <br><span class="text-color-primary"> with our reliable Web services.</span></h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="feature-list__two">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6 wow move-up">
                                            <div class="ht-box-icon style-02 single-svg-icon-box">
                                                <div class="icon-box-wrap">
                                                    <div class="icon">
                                                        <div class="svg-icon" id="svg-1" data-svg-icon="assets/images/svg/linea-basic-heart.svg"></div>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="heading">Website Design </h5>
                                                        <div class="text">We don’t only create websites that stand apart in their respective industries; we also give you the strategic edge with custom software development, mobile application development, proactive web maintenance, reliable web hosting and results-driven web coding.
                                                        </div>
                                                        <div class="feature-btn">
                                                            <a href="#">
                                                                <span class="button-text">Discover now</span>
                                                                <i class="far fa-long-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 wow move-up">
                                            <div class="ht-box-icon style-02 single-svg-icon-box">
                                                <div class="icon-box-wrap">
                                                    <div class="icon">
                                                        <div class="svg-icon" id="svg-2" data-svg-icon="assets/images/svg/linea-basic-case.svg"></div>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="heading">Graphic Design</h5>
                                                        <div class="text">Creating personalised design solutions for all aspects of your brand, ensuring consistent and coherent look & feel across all platforms. Branding that translates well from print to web without sacrificing on creativity.
                                                        </div>
                                                        <div class="feature-btn">
                                                            <a href="#">
                                                                <span class="button-text">Discover now</span>
                                                                <i class="far fa-long-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 wow move-up">
                                            <div class="ht-box-icon style-02 single-svg-icon-box">
                                                <!-- ht-box-icon Start -->
                                                <div class="icon-box-wrap">
                                                    <div class="icon">
                                                        <div class="svg-icon" id="svg-3" data-svg-icon="assets/images/svg/linea-basic-alarm.svg"></div>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="heading">Mobile App Development</h5>
                                                        <div class="text">Our mobile app solutions are created to suit the specific objectives of our clients, including seamless integration with existing information systems and brands.
                                                        </div>
                                                        <div class="feature-btn">
                                                            <a href="#">
                                                                <span class="button-text">Discover now</span>
                                                                <i class="far fa-long-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ht-box-icon End -->
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 wow move-up">
                                            <div class="ht-box-icon style-02 single-svg-icon-box">
                                                <!-- ht-box-icon Start -->
                                                <div class="icon-box-wrap">
                                                    <div class="icon">
                                                        <div class="svg-icon" id="svg-4" data-svg-icon="assets/images/svg/linea-basic-geolocalize-05.svg"></div>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="heading">E-commerce Development</h5>
                                                        <div class="text">Businesses now have the opportunity to engage with their clients by marketing, and selling their goods and services online, locally or globally. As a leading e-commerce website development company we have worked closely with numerous clients developing strategic, functional and unique-to-your business e-commerce websites.


                                                        </div>
                                                        <div class="feature-btn">
                                                            <a href="#">
                                                                <span class="button-text">Discover now</span>
                                                                <i class="far fa-long-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ht-box-icon End -->
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 wow move-up">
                                            <div class="ht-box-icon style-02 single-svg-icon-box">
                                                <!-- ht-box-icon Start -->
                                                <div class="icon-box-wrap">
                                                    <div class="icon">
                                                        <div class="svg-icon" id="svg-5" data-svg-icon="assets/images/svg/linea-ecommerce-money.svg"></div>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="heading">Systems Design</h5>
                                                        <div class="text">We have an experienced software development team made up of senior developers, testers and app developers who with their niche skills will create for you the best possible end product suited to your business.
                                                        </div>
                                                        <div class="feature-btn">
                                                            <a href="#">
                                                                <span class="button-text">Discover now</span>
                                                                <i class="far fa-long-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ht-box-icon End -->
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 wow move-up">
                                            <div class="ht-box-icon style-02 single-svg-icon-box">
                                                <!-- ht-box-icon Start -->
                                                <div class="icon-box-wrap">
                                                    <div class="icon">
                                                        <div class="svg-icon" id="svg-6" data-svg-icon="assets/images/svg/linea-basic-spread-text-bookmark.svg"></div>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="heading">SEO & Advanced Marketing</h5>
                                                        <div class="text">We have expertise in both on page SEO and off page SEO. On page SEO includes elements strategically placed into your website to help search engines recognise your site over others. On page SEO techniques include proper URL structures, user friendly navigation, the loading of fresh content, mobile friendly pages and fast loading pages
                                                        </div>
                                                        <div class="feature-btn">
                                                            <a href="#">
                                                                <span class="button-text">Discover now</span>
                                                                <i class="far fa-long-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ht-box-icon End -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--===========  feature-icon-wrapper  End =============-->



                <!--========= Pricing Table Area Start ==========-->
                <div class="pricing-table-area section-space--pb_100 bg-gradient">
                    <div class="pricing-table-title-area position-relative">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                                        <h6 class="section-sub-title mb-20">Pricing and Packages</h6>
                                        <h3 class="section-title">All Payements are  <span class="text-color-primary">once-off</span> </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pricing-table-content-area">
                        <div class="container">
                            <div class="row pricing-table-one">
                                <div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table wow move-up">
                                    <div class="pricing-table__inner">
                                        <div class="pricing-table__header">
                                            <h6 class="sub-title">Quick Starter</h6>
                                            <div class="pricing-table__image">
                                                <img src="assets/images/icons/mitech-pricing-box-icon-01-90x90.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="pricing-table__price-wrap">
                                                <h6 class="currency">R</h6>
                                                <h6 class="price">1250</h6>
                                                <h6 class="period"></h6>
                                            </div>
                                        </div>
                                        <div class="pricing-table__body">
                                            <div class="pricing-table__footer">
                                                <a href="#" class="ht-btn ht-btn-md ht-btn--outline">Get a Quote</a>
                                            </div>
                                            <ul class="pricing-table__list text-left">
                                                 <li>Responsive Website Design
for Mobile, Tab and PC</li>
                                                <li>1 pages</li>
                                                <li>Social Media Linking</li>
                                                <li>Contact Form</li>

                                                <li>Sliders & Picture Galleries</li>
                                                <li>Google Analytics & Reports</li>
                                                <li>5 - 10 Email accounts</li>
                                                <li>Basic SEO†</li>
                                                <li>Page titles, metadescriptions, Htags</li>
                                                <li>Image and link tags</li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table pricing-table--popular wow move-up">
                                    <div class="pricing-table__inner">
                                        <div class="pricing-table__feature-mark">
                                            <span>Popular Choice</span>
                                        </div>
                                        <div class="pricing-table__header">
                                            <h6 class="sub-title">Express Design</h6>
                                            <div class="pricing-table__image">
                                                <img src="assets/images/icons/mitech-pricing-box-icon-02-88x88.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="pricing-table__price-wrap">
                                                <h6 class="currency">R</h6>
                                                <h6 class="price">2150</h6>
                                                <h6 class="period"></h6>
                                            </div>
                                        </div>
                                        <div class="pricing-table__body">
                                            <div class="pricing-table__footer">
                                                <a href="#" class="ht-btn  ht-btn-md ">Get a Quote</a>
                                            </div>
                                            <ul class="pricing-table__list text-left">
                                                <li>Responsive Website Design
for Mobile, Tab and PC</li>
                                                <li>5-10 pages</li>
                                                <li>Social Media Linking</li>
                                                <li>Contact Form</li>

                                                <li>Sliders & Picture Galleries</li>
                                                <li>Google Analytics & Reports</li>
                                                <li>5 - 10 Email accounts</li>
                                                <li>Basic SEO†</li>
                                                <li>Page titles, metadescriptions, Htags</li>
                                                <li>Image and link tags</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table wow move-up">
                                    <div class="pricing-table__inner">
                                        <div class="pricing-table__header">
                                            <h6 class="sub-title">Unlimited Thoughts</h6>
                                            <div class="pricing-table__image">
                                                <img src="assets/images/icons/mitech-pricing-box-icon-03-90x90.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="pricing-table__price-wrap">
                                                <h6 class="currency">R</h6>
                                                <h6 class="price">4500</h6>
                                                <h6 class="period"></h6>
                                            </div>
                                        </div>
                                        <div class="pricing-table__body">
                                            <div class="pricing-table__footer">
                                                <a href="#" class="ht-btn ht-btn-md ht-btn--outline">Get a Quote</a>
                                            </div>
                                            <ul class="pricing-table__list text-left">
                                                <li>Responsive Website Design
for Mobile, Tab and PC</li>
                                                <li>Up to 20 pages</li>
                                                <li>Social Media Linking</li>
                                                <li>Contact Form</li>

                                                <li>Sliders & Picture Galleries</li>
                                                <li>Google Analytics & Reports</li>
                                                <li>Up to 30 Email accounts</li>
                                                <li>Basic SEO†</li>
                                                <li>Page titles, metadescriptions, Htags</li>
                                                <li>Image and link tags</li>
                                                <li>Dedicated Designer</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--========= Pricing Table Area End ==========-->


                <!--============ Contact Us Area Start =================-->
                <div class="contact-us-area infotechno-contact-us-bg section-space--pt_100">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7">
                                <div class="image">
                                    <img class="img-fluid" src="assets/images/banners/home-cybersecurity-contact-bg-image.png" alt="">
                                </div>
                            </div>

                            <div class="col-lg-4 ml-auto">
                                <div class="contact-info style-two text-left">


                                    <div class="contact-list-item">
                                        <a href="tel:+27678579459" class="single-contact-list">
                                            <div class="content-wrap">
                                                <div class="content">
                                                    <div class="icon">
                                                        <span class="fal fa-phone"></span>
                                                    </div>
                                                    <div class="main-content">
                                                        <h6 class="heading">Call Now for a Quote</h6>
                                                        <div class="text">+27678579459</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="mailto:design@droitix.co.za" class="single-contact-list">
                                            <div class="content-wrap">s
                                                <div class="content">
                                                    <div class="icon">
                                                        <span class="fal fa-envelope"></span>
                                                    </div>
                                                    <div class="main-content">
                                                        <h6 class="heading">Ask Anything</h6>
                                                        <div class="text">design@droitix.co.za</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--============ Contact Us Area End =================-->
            </div>
        </div>




        <!--====================  footer area ====================-->
        <div class="footer-area-wrapper reveal-footer bg-gray">
            <div class="footer-area section-space--ptb_80">
                <div class="container">
                    <div class="row footer-widget-wrapper">
                        <div class="col-lg-4 col-md-6 col-sm-6 footer-widget">
                            <div class="footer-widget__logo mb-30">
                                <img src="assets/images/logo/logo-dark.png" class="img-fluid" alt="">
                            </div>
                            <ul class="footer-widget__list">
                                <li>4 Magnolia Rd, Attlassville , Boksburg</li>
                                <li><a href="mailto:design@droitix.co.za" class="hover-style-link">design@droitix.co.za</a></li>
                                <li><a href="tel:123344556" class="hover-style-link text-black font-weight--bold">0678579459</a></li>

                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                            <h6 class="footer-widget__title mb-20">Web Design</h6>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                            <h6 class="footer-widget__title mb-20">Graphic Design</h6>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                            <h6 class="footer-widget__title mb-20">Support</h6>

                        </div>

                    </div>
                </div>
            </div>
            <div class="footer-copyright-area section-space--pb_30">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-center text-md-left">
                            <span class="copyright-text">&copy; 2021 Droitix Web. <a href="https://droitix.co.za/">All Rights Reserved.</a></span>
                        </div>
                        <div class="col-md-6 text-center text-md-right">
                            <ul class="list ht-social-networks solid-rounded-icon">

                                <li class="item">
                                    <a href="https://twitter.com/droitixweb" target="_blank" aria-label="Twitter" class="social-link hint--bounce hint--top hint--primary">
                                        <i class="fab fa-twitter link-icon"></i>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="https://facebook.com/droitixweb" target="_blank" aria-label="Facebook" class="social-link hint--bounce hint--top hint--primary">
                                        <i class="fab fa-facebook-f link-icon"></i>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="https://instagram.com/droitixweb" target="_blank" aria-label="Instagram" class="social-link hint--bounce hint--top hint--primary">
                                        <i class="fab fa-instagram link-icon"></i>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="https://linkedin.com/droitixweb" target="_blank" aria-label="Linkedin" class="social-link hint--bounce hint--top hint--primary">
                                        <i class="fab fa-linkedin link-icon"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of footer area  ====================-->





    </div>
    <!--====================  scroll top ====================-->
    <a href="#" class="scroll-top" id="scroll-top">
        <i class="arrow-top fal fa-long-arrow-up"></i>
        <i class="arrow-bottom fal fa-long-arrow-up"></i>
    </a>
    <!--====================  End of scroll top  ====================-->
    <!-- Start Toolbar -->
    <div class="demo-option-container">

        <!-- Start Quick Link -->
        <div class="demo-option-wrapper">
            <div class="demo-panel-header">
                <div class="title">
                    <h6 class="heading mt-30">IT Solutions Mitech - Technology, IT Solutions & Services Html5 Template</h6>
                </div>

                <div class="panel-btn mt-20">
                    <a class="ht-btn ht-btn-md" href="https://themeforest.net/item/mitech-it-solutions-html-template/24906742?ref=AslamHasib"><i class="far fa-shopping-cart mr-2"></i> Buy Now </a>
                </div>
            </div>
            <div class="demo-quick-option-list">
                <a class="link hint--bounce hint--black hint--top hint--dark" href="index-appointment.html" aria-label="Appointment">
                    <img class="img-fluid" src="assets/images/demo-images/home-01.jpg" alt="Images">
                </a>
                <a class="link hint--bounce hint--black hint--top hint--dark" href="index-infotechno.html" aria-label="Infotechno">
                    <img class="img-fluid" src="assets/images/demo-images/home-02.jpg" alt="Images">
                </a>
                <a class="link hint--bounce hint--black hint--top hint--dark" href="index-processing.html" aria-label="Processing">
                    <img class="img-fluid" src="assets/images/demo-images/home-03.jpg" alt="Images">
                </a>
                <a class="link hint--bounce hint--black hint--top hint--dark" href="index-services.html" aria-label="Services">
                    <img class="img-fluid" src="assets/images/demo-images/home-04.jpg" alt="Images">
                </a>
                <a class="link hint--bounce hint--black hint--top hint--dark" href="index-resolutions.html" aria-label="Resolutions">
                    <img class="img-fluid" src="assets/images/demo-images/home-05.jpg" alt="Images">
                </a>
                <a class="link hint--bounce hint--black hint--top hint--dark" href="index-cybersecurity.html" aria-label="Cybersecurity">
                    <img class="img-fluid" src="assets/images/demo-images/home-06.jpg" alt="Images">
                </a>
            </div>
        </div>
        <!-- End Quick Link -->
    </div>
    <!-- End Toolbar -->

    <!--====================  mobile menu overlay ====================-->
    <div class="mobile-menu-overlay" id="mobile-menu-overlay">
        <div class="mobile-menu-overlay__inner">
            <div class="mobile-menu-overlay__header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-8">
                            <!-- logo -->
                            <div class="logo">
                                <a href="index.html">
                                    <img src="/assets/images/logo/logo-dark.png" class="img-fluid" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-4">
                            <!-- mobile menu content -->
                            <div class="mobile-menu-content text-right">
                                <span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-menu-overlay__body">
                <nav class="offcanvas-navigation">
                  <ul>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Home</span></a>

                                            </li>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Website Design</span></a>

                                            </li>

                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Graphic Designing</span></a>

                                            </li>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Web Systems Design</span></a>

                                            </li>
                                                  <li class="has-children has-children--multilevel-submenu">
                                                <a href="#"><span>Online Stores</span></a>

                                            </li>

                                        </ul>
                </nav>
            </div>
        </div>
    </div>
    <!--====================  End of mobile menu overlay  ====================-->







    <!--====================  search overlay ====================-->
    <div class="search-overlay" id="search-overlay">

        <div class="search-overlay__header">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-6 ml-auto col-4">
                        <!-- search content -->
                        <div class="search-content text-right">
                            <span class="mobile-navigation-close-icon" id="search-close-trigger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-overlay__inner">
            <div class="search-overlay__body">
                <div class="search-overlay__form">
                    <form action="#">
                        <input type="text" placeholder="Search">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of search overlay  ====================-->

    <!-- JS
    ============================================ -->
    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>

    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.5.1.min.js"></script>
    <script src="assets/js/vendor/jquery-migrate-3.3.0.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>

    <!-- wow JS -->
    <!-- <script src="assets/js/plugins/wow.min.js"></script> -->

    <!-- Swiper Slider JS -->
    <!-- <script src="assets/js/plugins/swiper.min.js"></script> -->

    <!-- Light gallery JS -->
    <!-- <script src="assets/js/plugins/lightgallery.min.js"></script> -->

    <!-- Waypoints JS -->
    <!-- <script src="assets/js/plugins/waypoints.min.js"></script> -->

    <!-- Counter down JS -->
    <!-- <script src="assets/js/plugins/countdown.min.js"></script> -->

    <!-- Isotope JS -->
    <!-- <script src="assets/js/plugins/isotope.min.js"></script> -->

    <!-- Masonry JS -->
    <!-- <script src="assets/js/plugins/masonry.min.js"></script> -->

    <!-- ImagesLoaded JS -->
    <!-- <script src="assets/js/plugins/images-loaded.min.js"></script> -->

    <!-- Wavify JS -->
    <!-- <script src="assets/js/plugins/wavify.js"></script> -->

    <!-- jQuery Wavify JS -->
    <!-- <script src="assets/js/plugins/jquery.wavify.js"></script> -->

    <!-- circle progress JS -->
    <!-- <script src="assets/js/plugins/circle-progress.min.js"></script> -->

    <!-- counterup JS -->
    <!-- <script src="assets/js/plugins/counterup.min.js"></script> -->


    <!-- animation text JS -->
    <!-- <script src="assets/js/plugins/animation-text.min.js"></script> -->

    <!-- Vivus JS -->
    <!-- <script src="assets/js/plugins/vivus.min.js"></script> -->

    <!-- Some plugins JS -->
    <!-- <script src="assets/js/plugins/some-plugins.js"></script> -->


    <!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->


    <script src="assets/js/plugins/plugins.min.js"></script>


    <!-- Main JS -->
    <script src="assets/js/main.js"></script>


</body>


<!-- Mirrored from demo.hasthemes.com/mitech-preview/index-cybersecurity.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Feb 2021 14:34:37 GMT -->
</html>
